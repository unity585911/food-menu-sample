using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FoodRowView : MonoBehaviour
{
    public Image IconImage;
    public TMP_Text TitleText;
    [SerializeField] private Button _button;
    private Food _food;
    public Food Food 
    {
        get => _food;
        set
        {
            _food = value;
        }
    }

    private void Start()
    {
        _button.onClick.AddListener(ShowDetailFood);
    }

    private void ShowDetailFood()
    {
        InformationPopup.Instance.Show(Food.description);
    }
}
