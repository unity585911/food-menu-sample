using UnityEngine;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Menu _menu;
    [SerializeField] private GameObject _foodRowPrefab;
    [SerializeField] private Transform _parentTransform;

    // Start is called before the first frame update
    void Start()
    {
        foreach(var food in _menu.foods) 
        {
            var foodRow = Instantiate(_foodRowPrefab, _parentTransform);
            foodRow.GetComponent<FoodRowView>().IconImage.sprite = food.icon;
            foodRow.GetComponent<FoodRowView>().TitleText.text = food.title;
            foodRow.GetComponent<FoodRowView>().Food = food;
        }
    }
}
